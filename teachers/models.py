from django.db import models
from faker import Faker
import datetime
import random
from django.core.validators import MinValueValidator, MaxValueValidator, \
    RegexValidator
from django.urls import reverse


from core_lms.validators import even_integer_validator


class Teacher(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.TextField(max_length=64, null=False)
    invite_reason = models.CharField(max_length=64)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=40, default='str')
    phone_number = models.CharField(max_length=30, default='str')
    birth_date = models.CharField(max_length=50, default='str')
    phone_number = models.CharField(
        max_length=24,
        validators=[
            RegexValidator(
                r'^(\+\d\d?)?\(\d{3}\)(\d-?){7}$',
                message="Phone number should be in format +1(111)222-33-44"
            )
        ]
    )
    enroll_date = models.DateField(default=datetime.datetime.today())
    graduate_date = models.DateField(default=datetime.datetime.today)

    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)

    @classmethod
    def generate_teachers(cls, count):
        faker = Faker()

        for _ in range(count):
            s = Teacher()
            s.first_name = faker.first_name()
            s.last_name = faker.last_name()
            s.email = faker.email()

            s.save()

    def __str__(self):
        return f"Student({self.id}) {self.first_name} {self.last_name} " \
               f"{self.age} {self.phone_number} " \
               f"{self.enroll_date} {self.graduate_date}"


    @property
    def name(self):
        return f'{self.first_name} {self.last_name}'

    def get_update_link(self):
        return reverse('students:update_students',kwargs={'id':self.id})








