from django.urls import path
from .views import get_teachers, create_teachers, update_teachers, delete_teachers

urlpatterns = [
    path('', get_teachers, name='list_teachers'),
    path('create', create_teachers, name='create_teachers'),
    path('update/<int:id>', update_teachers, name='update_teachers'),
    path('delete/<int:id>', delete_teachers, name='delete_teachers'),
]
