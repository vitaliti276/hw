from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from .models import Teacher
from .forms import TeacherCreateForm, TeacherUpdateForm, TeacherFilter


def get_teachers(request):
    qs = Teacher.objects.all()

    qs = qs.order_by('-id')

    teachers_filter = TeacherFilter(data=request.GET, queryset=qs)

    return render(request, 'list_teachers.html', {
        'args': request.GET,
        'filter': teachers_filter
    })


@csrf_exempt
def create_teachers(request):
    if request.method == 'POST':
        form = TeacherCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('list_teachers'))
    else:
        form = TeacherCreateForm()

    return render(request, 'create_teacher.html', {
        'form': form
    })


@csrf_exempt
def update_teachers(request, id):
    teacher = get_object_or_404(Teacher, id=id)

    if request.method == 'POST':
        form = TeacherUpdateForm(request.POST, instance=teacher)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('teachers:list_teachers'))
    else:
        form = TeacherUpdateForm(instance=teacher)

    return render(request, 'edit_teacher.html', {
        'form': form
    })


@csrf_exempt
def delete_teachers(request, id):
    teachers = get_object_or_404(Teacher, id=id)

    if request.method == 'POST':
        teacher.delete()
        return HttpResponseRedirect(reverse('teachers:list_teachers'))

    return render(
        request,
        'delete_teacher.html',
        {
            'teacher': teacher
        }
    )


# Not used
def delete_teacher_no_confirmation(request, id):
    try:
        teacher = Teacher.objects.get(id=id)
        teacher.delete()
    except Teacher.DoesNotExist:
        pass
    return HttpResponseRedirect(reverse('teachers:list_teachers'))

































