import django_filters
from django.forms import ModelForm
import re

from django.core.exceptions import ValidationError
from .models import Teacher
from django.forms import ModelForm



class TeacherFilter(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name']


class TeacherBaseForm(ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'

    def clean(self):
        result = super().clean()

        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_date']

        if enroll_date > graduate_date:
            raise ValidationError("Enroll date cannot be less "
                                  "than gradate date")

        return result

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']

        has_phone_number_qs = Teacher.objects.filter(
            phone_number=phone_number
        )

        if self.instance:
            has_phone_number_qs = has_phone_number_qs.exclude(
                id=self.instance.id
            )

        if has_phone_number_qs.exists():
            raise ValidationError("Phone number is not unique")

        return phone_number


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta:
        model = Teacher
        exclude = ['age']
